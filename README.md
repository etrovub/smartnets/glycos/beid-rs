# beid-rs
This project aims to offer a Rusty abstraction layer over the [official eid-mw C library](https://github.com/Fedict/eid-mw/).

Most of the (working) examples in the official library have been ported and can be viewed in the examples/ directory.

## Implemented functionality
- View card slots
- Start sessions
- Get data from a card
- Get mechanisms and info
- Perform a signature


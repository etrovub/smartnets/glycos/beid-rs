#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use std::error;
use std::fmt;

#[derive(Debug)]
pub enum Error {
    PKCS11_Error(PKCS11_Error),
    DL_Error(libloading::Error),
}

//TODO: See if it's possible to do this type alias
//pub type Result<T> = std::result::Result<T, Error>;

//TODO: Define all CK_RV's as messages

pub struct PKCS11_Error(CK_RV);

impl PKCS11_Error {
    /// Turn self into a Result based on its value
    pub fn ret(self) -> Result<(), Error> {
        match self.0 {
            0 => Ok(()),
            _ => Err(Error::PKCS11_Error(self)),
        }
    }

    pub fn ret_with<T>(self, val: T) -> Result<T, Error> {
        match self.0 {
            0 => Ok(val),
            _ => Err(Error::PKCS11_Error(self)),
        }
    }
}

impl From<u64> for PKCS11_Error {
    fn from(e: u64) -> Self {
        PKCS11_Error(e)
    }
}

impl fmt::Display for PKCS11_Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error {}: {}", self.0, self.parse())
    }
}
impl fmt::Debug for PKCS11_Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error {}: {}", self.0, self.parse())
    }
}
impl error::Error for PKCS11_Error {}

/// Return value definitions
impl PKCS11_Error {
    pub fn parse(&self) -> &str {
        match self.0 as u32 {
            CKR_OK => "CKR_OK",
            CKR_CANCEL => "CKR_CANCEL",
            CKR_HOST_MEMORY => "CKR_HOST_MEMORY",
            CKR_SLOT_ID_INVALID => "CKR_SLOT_ID_INVALID",
            CKR_GENERAL_ERROR => "CKR_GENERAL_ERROR",
            CKR_FUNCTION_FAILED => "CKR_FUNCTION_FAILED",
            CKR_ARGUMENTS_BAD => "CKR_ARGUMENTS_BAD",
            CKR_NO_EVENT => "CKR_NO_EVENT",
            CKR_NEED_TO_CREATE_THREADS => "CKR_NEED_TO_CREATE_THREADS",
            CKR_CANT_LOCK => "CKR_CANT_LOCK",
            CKR_ATTRIBUTE_READ_ONLY => "CKR_ATTRIBUTE_READ_ONLY",
            CKR_ATTRIBUTE_SENSITIVE => "CKR_ATTRIBUTE_SENSITIVE",
            CKR_ATTRIBUTE_TYPE_INVALID => "CKR_ATTRIBUTE_TYPE_INVALID",
            CKR_ATTRIBUTE_VALUE_INVALID => "CKR_ATTRIBUTE_VALUE_INVALID",
            CKR_DATA_INVALID => "CKR_DATA_INVALID",
            CKR_DATA_LEN_RANGE => "CKR_DATA_LEN_RANGE",
            CKR_DEVICE_ERROR => "CKR_DEVICE_ERROR",
            CKR_DEVICE_MEMORY => "CKR_DEVICE_MEMORY",
            CKR_DEVICE_REMOVED => "CKR_DEVICE_REMOVED",
            CKR_ENCRYPTED_DATA_INVALID => "CKR_ENCRYPTED_DATA_INVALID",
            CKR_ENCRYPTED_DATA_LEN_RANGE => "CKR_ENCRYPTED_DATA_LEN_RANGE",
            CKR_FUNCTION_CANCELED => "CKR_FUNCTION_CANCELED",
            CKR_FUNCTION_NOT_PARALLEL => "CKR_FUNCTION_NOT_PARALLEL",
            CKR_FUNCTION_NOT_SUPPORTED => "CKR_FUNCTION_NOT_SUPPORTED",
            CKR_KEY_HANDLE_INVALID => "CKR_KEY_HANDLE_INVALID",
            CKR_KEY_SIZE_RANGE => "CKR_KEY_SIZE_RANGE",
            CKR_KEY_TYPE_INCONSISTENT => "CKR_KEY_TYPE_INCONSISTENT",
            CKR_KEY_NOT_NEEDED => "CKR_KEY_NOT_NEEDED",
            CKR_KEY_CHANGED => "CKR_KEY_CHANGED",
            CKR_KEY_NEEDED => "CKR_KEY_NEEDED",
            CKR_KEY_INDIGESTIBLE => "CKR_KEY_INDIGESTIBLE",
            CKR_KEY_FUNCTION_NOT_PERMITTED => "CKR_KEY_FUNCTION_NOT_PERMITTED",
            CKR_KEY_NOT_WRAPPABLE => "CKR_KEY_NOT_WRAPPABLE",
            CKR_KEY_UNEXTRACTABLE => "CKR_KEY_UNEXTRACTABLE",
            CKR_MECHANISM_INVALID => "CKR_MECHANISM_INVALID",
            CKR_MECHANISM_PARAM_INVALID => "CKR_MECHANISM_PARAM_INVALID",
            CKR_OBJECT_HANDLE_INVALID => "CKR_OBJECT_HANDLE_INVALID",
            CKR_OPERATION_ACTIVE => "CKR_OPERATION_ACTIVE",
            CKR_OPERATION_NOT_INITIALIZED => "CKR_OPERATION_NOT_INITIALIZED",
            CKR_PIN_INCORRECT => "CKR_PIN_INCORRECT",
            CKR_PIN_INVALID => "CKR_PIN_INVALID",
            CKR_PIN_LEN_RANGE => "CKR_PIN_LEN_RANGE",
            CKR_PIN_EXPIRED => "CKR_PIN_EXPIRED",
            CKR_PIN_LOCKED => "CKR_PIN_LOCKED",
            CKR_SESSION_CLOSED => "CKR_SESSION_CLOSED",
            CKR_SESSION_COUNT => "CKR_SESSION_COUNT",
            CKR_SESSION_HANDLE_INVALID => "CKR_SESSION_HANDLE_INVALID",
            CKR_SESSION_PARALLEL_NOT_SUPPORTED => "CKR_SESSION_PARALLEL_NOT_SUPPORTED",
            CKR_SESSION_READ_ONLY => "CKR_SESSION_READ_ONLY",
            CKR_SESSION_EXISTS => "CKR_SESSION_EXISTS",
            CKR_SESSION_READ_ONLY_EXISTS => "CKR_SESSION_READ_ONLY_EXISTS",
            CKR_SESSION_READ_WRITE_SO_EXISTS => "CKR_SESSION_READ_WRITE_SO_EXISTS",
            CKR_SIGNATURE_INVALID => "CKR_SIGNATURE_INVALID",
            CKR_SIGNATURE_LEN_RANGE => "CKR_SIGNATURE_LEN_RANGE",
            CKR_TEMPLATE_INCOMPLETE => "CKR_TEMPLATE_INCOMPLETE",
            CKR_TEMPLATE_INCONSISTENT => "CKR_TEMPLATE_INCONSISTENT",
            CKR_TOKEN_NOT_PRESENT => "CKR_TOKEN_NOT_PRESENT",
            CKR_TOKEN_NOT_RECOGNIZED => "CKR_TOKEN_NOT_RECOGNIZED",
            CKR_TOKEN_WRITE_PROTECTED => "CKR_TOKEN_WRITE_PROTECTED",
            CKR_UNWRAPPING_KEY_HANDLE_INVALID => "CKR_UNWRAPPING_KEY_HANDLE_INVALID",
            CKR_UNWRAPPING_KEY_SIZE_RANGE => "CKR_UNWRAPPING_KEY_SIZE_RANGE",
            CKR_UNWRAPPING_KEY_TYPE_INCONSISTENT => "CKR_UNWRAPPING_KEY_TYPE_INCONSISTENT",
            CKR_USER_ALREADY_LOGGED_IN => "CKR_USER_ALREADY_LOGGED_IN",
            CKR_USER_NOT_LOGGED_IN => "CKR_USER_NOT_LOGGED_IN",
            CKR_USER_PIN_NOT_INITIALIZED => "CKR_USER_PIN_NOT_INITIALIZED",
            CKR_USER_TYPE_INVALID => "CKR_USER_TYPE_INVALID",
            CKR_USER_ANOTHER_ALREADY_LOGGED_IN => "CKR_USER_ANOTHER_ALREADY_LOGGED_IN",
            CKR_USER_TOO_MANY_TYPES => "CKR_USER_TOO_MANY_TYPES",
            CKR_WRAPPED_KEY_INVALID => "CKR_WRAPPED_KEY_INVALID",
            CKR_WRAPPED_KEY_LEN_RANGE => "CKR_WRAPPED_KEY_LEN_RANGE",
            CKR_WRAPPING_KEY_HANDLE_INVALID => "CKR_WRAPPING_KEY_HANDLE_INVALID",
            CKR_WRAPPING_KEY_SIZE_RANGE => "CKR_WRAPPING_KEY_SIZE_RANGE",
            CKR_WRAPPING_KEY_TYPE_INCONSISTENT => "CKR_WRAPPING_KEY_TYPE_INCONSISTENT",
            CKR_RANDOM_SEED_NOT_SUPPORTED => "CKR_RANDOM_SEED_NOT_SUPPORTED",
            CKR_RANDOM_NO_RNG => "CKR_RANDOM_NO_RNG",
            CKR_DOMAIN_PARAMS_INVALID => "CKR_DOMAIN_PARAMS_INVALID",
            CKR_BUFFER_TOO_SMALL => "CKR_BUFFER_TOO_SMALL",
            CKR_SAVED_STATE_INVALID => "CKR_SAVED_STATE_INVALID",
            CKR_INFORMATION_SENSITIVE => "CKR_INFORMATION_SENSITIVE",
            CKR_STATE_UNSAVEABLE => "CKR_STATE_UNSAVEABLE",
            CKR_CRYPTOKI_NOT_INITIALIZED => "CKR_CRYPTOKI_NOT_INITIALIZED",
            CKR_CRYPTOKI_ALREADY_INITIALIZED => "CKR_CRYPTOKI_ALREADY_INITIALIZED",
            CKR_MUTEX_BAD => "CKR_MUTEX_BAD",
            CKR_MUTEX_NOT_LOCKED => "CKR_MUTEX_NOT_LOCKED",
            CKR_FUNCTION_REJECTED => "CKR_FUNCTION_REJECTED",
            CKR_VENDOR_DEFINED => "CKR_VENDOR_DEFINED",
            _ => "Illegal error message",
        }
    }
}

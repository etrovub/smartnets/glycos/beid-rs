#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(clippy::unused_unit)]
#![allow(unused)]
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#![allow(unused)]
mod data_types;
pub use data_types::*;

pub mod errors;
use crate::errors::{Error, PKCS11_Error};

mod bindings;
use bindings::*;

pub mod files;

/// # Interface
pub struct BeID {
    lib: libloading::Library,
}

impl BeID {
    /// Create a new library
    pub fn new() -> Result<Self, Error> {
        let name = std::str::from_utf8(PKCS11_LIB).unwrap();
        unsafe { libloading::Library::new(name) }
            .map_err(|e| Error::DL_Error(e))
            .map(|l| Self { lib: l })
    }
}

/// Macro for defining functions
/// Escapes the function if there is a libloading error
///
/// Syntax:
/// c_func!(arg1, arg2, ... -> return_type, function_name, self)
macro_rules! c_func {
    ($($args:ident),*, $nam:literal, $interface:expr) => {
        {
            let func: Result<
                libloading::Symbol<unsafe extern "C" fn($($args),*) -> CK_RV>,
                libloading::Error
            > = $interface.lib.get($nam);
            match func {
                Err(x) => return Err(Error::DL_Error(x)),
                Ok(ok) => ok,
            }
        }
    }
}

use std::ptr;
impl BeID {
    /// Load the library
    pub fn initialize(&self) -> Result<(), Error> {
        let retval = unsafe {
            let func = c_func!(CK_C_INITIALIZE_ARGS_PTR, b"C_Initialize", self);
            func(ptr::null_mut())
        };
        PKCS11_Error::from(retval).ret()
    }

    /// Finalize. Called by drop.
    fn finalize(&self) -> Result<(), Error> {
        let retval = unsafe {
            // Argument is reserved, so such be NULL for now
            let func = c_func!(CK_VOID_PTR, b"C_Finalize", self);
            func(ptr::null_mut())
        };
        PKCS11_Error::from(retval).ret()
    }

    /// Get number of slots
    ///
    /// Arguments:
    /// - token_present: if true, only lists slots with a token (card) present
    pub fn get_number_of_slots(&self, token_present: bool) -> Result<usize, Error> {
        let (retval, slot_count) = unsafe {
            let func = c_func!(
                CK_BBOOL,
                CK_SLOT_ID_PTR,
                CK_ULONG_PTR,
                b"C_GetSlotList",
                self
            );
            let slot_count_ptr = &mut 0u64 as CK_ULONG_PTR;
            (
                func(token_present as u8, ptr::null_mut(), slot_count_ptr),
                *slot_count_ptr as usize,
            )
        };
        PKCS11_Error::from(retval).ret_with(slot_count)
    }

    /// Get slots
    ///
    /// Arguments:
    /// - token_present: if true, only lists slots with a token (card) present
    pub fn get_slot_list(&self, token_present: bool) -> Result<Vec<CK_SLOT_ID>, Error> {
        let slot_count = self.get_number_of_slots(token_present)?;
        let (retval, slots) = unsafe {
            let func = c_func!(
                CK_BBOOL,
                CK_SLOT_ID_PTR,
                CK_ULONG_PTR,
                b"C_GetSlotList",
                self
            );
            let mut slots = vec![0 as CK_SLOT_ID; slot_count];
            let _slot_count_ptr = &mut (slot_count as CK_ULONG) as CK_ULONG_PTR;
            (
                func(token_present as u8, slots.as_mut_ptr(), _slot_count_ptr),
                slots,
            )
        };
        PKCS11_Error::from(retval).ret_with(slots)
    }

    /// Get size of list of mechanisms for a slot
    fn get_mechanism_list_size(&self, slot: CK_SLOT_ID) -> Result<usize, Error> {
        let (retval, no_mechanisms) = unsafe {
            let func = c_func!(
                CK_SLOT_ID,
                CK_MECHANISM_TYPE_PTR,
                CK_ULONG_PTR,
                b"C_GetMechanismList",
                self
            );
            let no_mechanisms = &mut 0u64 as CK_ULONG_PTR;
            (func(slot, ptr::null_mut(), no_mechanisms), *no_mechanisms)
        };
        PKCS11_Error::from(retval).ret_with(no_mechanisms as usize)
    }

    /// Get list of mechanisms for a slot
    pub fn get_mechanism_list(&self, slot: CK_SLOT_ID) -> Result<Vec<CK_MECHANISM_TYPE>, Error> {
        let mut size = self.get_mechanism_list_size(slot)? as CK_ULONG;
        let (retval, mechanisms) = unsafe {
            let func = c_func!(
                CK_SLOT_ID,
                CK_MECHANISM_TYPE_PTR,
                CK_ULONG_PTR,
                b"C_GetMechanismList",
                self
            );
            let mut mechanisms = vec![0 as CK_MECHANISM_TYPE; size as usize];
            (func(slot, mechanisms.as_mut_ptr(), &mut size), mechanisms)
        };
        PKCS11_Error::from(retval).ret_with(mechanisms)
    }

    /// Get mechanism info
    pub fn get_mechanism_info(
        &self,
        slot: CK_SLOT_ID,
        mechanism: CK_MECHANISM_TYPE,
    ) -> Result<CK_MECHANISM_INFO, Error> {
        let (retval, info) = unsafe {
            let func = c_func!(
                CK_SLOT_ID,
                CK_MECHANISM_TYPE,
                CK_MECHANISM_INFO_PTR,
                b"C_GetMechanismInfo",
                self
            );
            let mut info = CK_MECHANISM_INFO {
                ulMinKeySize: 0,
                ulMaxKeySize: 0,
                flags: 0,
            };
            (func(slot, mechanism, &mut info), info)
        };
        PKCS11_Error::from(retval).ret_with(info)
    }

    /// Get token info
    pub fn get_token_info(&self, slot: CK_SLOT_ID) -> Result<CK_TOKEN_INFO, Error> {
        let (retval, info) = unsafe {
            let func = c_func!(CK_SLOT_ID, CK_TOKEN_INFO_PTR, b"C_GetTokenInfo", self);
            let mut info = CK_TOKEN_INFO::default();
            (func(slot, &mut info), info)
        };
        PKCS11_Error::from(retval).ret_with(info)
    }

    /// Open a session
    pub fn open_session<'a>(
        &'a self,
        slot_id: CK_SLOT_ID,
        flags: SessionFlags,
    ) -> Result<Session<'a>, Error> {
        let (retval, handle) = unsafe {
            let func = c_func!(
                CK_SLOT_ID,
                CK_FLAGS,
                CK_VOID_PTR,
                CK_NOTIFY,
                CK_SESSION_HANDLE_PTR,
                b"C_OpenSession",
                self
            );
            // TODO: decent callback
            let _p_application = ptr::null_mut() as CK_VOID_PTR;
            let _notify = None;
            let handle_ptr = &mut 0u64 as CK_SESSION_HANDLE_PTR;
            (
                func(
                    slot_id,
                    flags.bits() as u64,
                    _p_application,
                    _notify,
                    handle_ptr,
                ),
                *handle_ptr,
            )
        };
        PKCS11_Error::from(retval).ret_with(Session {
            interface: &self,
            handle,
        })
    }
}

/// Finalize on drop
impl Drop for BeID {
    fn drop(&mut self) {
        self.finalize().unwrap();
    }
}

/// # Session
pub struct Session<'a> {
    interface: &'a BeID,
    handle: CK_SESSION_HANDLE,
}

impl<'a> Session<'a> {
    /// Close the session. Called by `drop`.
    fn close(&mut self) -> Result<(), Error> {
        let retval = unsafe {
            let func = c_func!(CK_SESSION_HANDLE, b"C_CloseSession", self.interface);
            func(self.handle)
        };
        PKCS11_Error::from(retval).ret()
    }

    /// Initialize object search according to template
    fn find_objects_init(&self, template: &mut Vec<CK_ATTRIBUTE>) -> Result<(), Error> {
        let retval = unsafe {
            let func = c_func!(
                CK_SESSION_HANDLE,
                CK_ATTRIBUTE_PTR,
                CK_ULONG,
                b"C_FindObjectsInit",
                self.interface
            );
            func(
                self.handle,
                template.as_mut_ptr(),
                template.len() as CK_ULONG,
            )
        };
        PKCS11_Error::from(retval).ret()
    }

    /// Search the objects
    fn find_objects(&self, max_objects: usize) -> Result<Vec<CK_OBJECT_HANDLE>, Error> {
        let (retval, mut objects, actual_number) = unsafe {
            let func = c_func!(
                CK_SESSION_HANDLE,
                CK_OBJECT_HANDLE_PTR,
                CK_ULONG,
                CK_ULONG_PTR,
                b"C_FindObjects",
                self.interface
            );
            let mut objects = vec![0 as CK_OBJECT_HANDLE; max_objects];
            let actual_number = &mut 0u64 as CK_ULONG_PTR;
            (
                func(
                    self.handle,
                    objects.as_mut_ptr(),
                    max_objects as CK_ULONG,
                    actual_number,
                ),
                objects,
                *actual_number,
            )
        };
        objects.truncate(actual_number as usize);
        PKCS11_Error::from(retval).ret_with(objects)
    }

    /// Finalize finding
    fn find_objects_final(&self) -> Result<(), Error> {
        let retval = unsafe {
            let func = c_func!(CK_SESSION_HANDLE, b"C_FindObjectsFinal", self.interface);
            func(self.handle)
        };
        PKCS11_Error::from(retval).ret()
    }

    /// Initialize, do and finialize an object search
    pub fn search_objects(
        &self,
        mut template: SearchTemplate,
        max_objects: usize,
    ) -> Result<Vec<CK_OBJECT_HANDLE>, Error> {
        self.find_objects_init(&mut template.attributes)?;
        let objects = self.find_objects(max_objects)?;
        self.find_objects_final();
        Ok(objects)
    }

    /// Get the length of the value of the attribute
    fn get_attribute_value_length(&self, object_handle: CK_OBJECT_HANDLE) -> Result<usize, Error> {
        let (retval, l) = unsafe {
            let func = c_func!(
                CK_SESSION_HANDLE,
                CK_OBJECT_HANDLE,
                CK_ATTRIBUTE_PTR,
                CK_ULONG,
                b"C_GetAttributeValue",
                self.interface
            );
            let mut template = data_types::ObjectLengthTemplate::new();
            (
                func(self.handle, object_handle, template.as_mut_ptr(), 1),
                template[0].ulValueLen as usize,
            )
        };
        PKCS11_Error::from(retval).ret_with(l)
    }

    /// Get the value of the attribute
    pub fn get_attribute_value(&self, object_handle: CK_OBJECT_HANDLE) -> Result<Vec<u8>, Error> {
        let l = self.get_attribute_value_length(object_handle)?;
        let mut object = vec![0 as CK_BYTE; l];
        let retval = unsafe {
            let func = c_func!(
                CK_SESSION_HANDLE,
                CK_OBJECT_HANDLE,
                CK_ATTRIBUTE_PTR,
                CK_ULONG,
                b"C_GetAttributeValue",
                self.interface
            );
            let object_ptr = object.as_mut_ptr() as CK_VOID_PTR;
            let mut template = data_types::ObjectTemplate::new(object_ptr, l);
            let retval = func(
                self.handle,
                object_handle,
                template.as_mut_ptr(),
                template.len() as u64,
            );
            retval
        };
        PKCS11_Error::from(retval).ret_with(object)
    }

    /// Initialize Signature
    pub fn sign_init(
        &self,
        object: CK_OBJECT_HANDLE,
        algorithm: SignatureAlgorithm,
    ) -> Result<(), Error> {
        let mut mechanism = match algorithm {
            SignatureAlgorithm::ECDSA(_) => data_types::ECDSAMechanism::new(),
        };
        let retval = unsafe {
            let func = c_func!(
                CK_SESSION_HANDLE,
                CK_MECHANISM_PTR,
                CK_OBJECT_HANDLE,
                b"C_SignInit",
                self.interface
            );
            let mech_ptr = &mut mechanism as CK_MECHANISM_PTR;
            func(self.handle, mech_ptr, object)
        };
        PKCS11_Error::from(retval).ret()
    }

    /// Sign
    pub fn sign(
        &self,
        data: &mut Vec<u8>,
        algorithm: SignatureAlgorithm,
    ) -> Result<Vec<u8>, Error> {
        let (retval, mut sign, sign_length) = unsafe {
            let func = c_func!(
                CK_SESSION_HANDLE,
                CK_BYTE_PTR,
                CK_ULONG,
                CK_BYTE_PTR,
                CK_ULONG_PTR,
                b"C_Sign",
                self.interface
            );
            let sign_length = match algorithm {
                SignatureAlgorithm::ECDSA(n) => n,
            };
            let mut sign = vec![0u8; sign_length];
            let mut sign_length = sign_length as CK_ULONG;
            (
                func(
                    self.handle,
                    data.as_mut_ptr(),
                    data.len() as CK_ULONG,
                    sign.as_mut_ptr(),
                    &mut sign_length as CK_ULONG_PTR,
                ),
                sign,
                sign_length,
            )
        };
        sign.truncate(sign_length as usize);
        /// TODO: Finalize signature
        PKCS11_Error::from(retval).ret_with(sign)
    }

    /// Get session info
    pub fn get_info(&self) -> Result<CK_SESSION_INFO, Error> {
        let (retval, info) = unsafe {
            let func = c_func!(
                CK_SESSION_HANDLE,
                CK_SESSION_INFO_PTR,
                b"C_GetSessionInfo",
                self.interface
            );
            let mut info = CK_SESSION_INFO::default();
            (func(self.handle, &mut info), info)
        };
        PKCS11_Error::from(retval).ret_with(info)
    }

    /// Login
    ///
    /// Arguments:
    /// - user_type
    /// - pin: Option if PIN is needed. Pin is not needed when the reader itself
    ///        asks for it, or when other authentication methods are used.
    ///        The TokenInfo flag ??? should be checked to see if PIN is needed.
    pub fn login(&self, user_type: UserType, pin: Option<&mut Vec<u8>>) -> Result<(), Error> {
        let retval = unsafe {
            let func = c_func!(
                CK_SESSION_HANDLE,
                CK_USER_TYPE,
                CK_UTF8CHAR_PTR,
                CK_ULONG,
                b"C_Login",
                self.interface
            );
            let (pin_ptr, pin_len) = match pin {
                Some(x) => (x.as_mut_ptr(), x.len()),
                None => (ptr::null_mut(), 0),
            };
            func(self.handle, user_type.c_val(), pin_ptr, pin_len as CK_ULONG)
        };
        PKCS11_Error::from(retval).ret()
    }
}

/// Close session on drop
impl<'a> Drop for Session<'a> {
    fn drop(&mut self) {
        self.close().unwrap();
    }
}

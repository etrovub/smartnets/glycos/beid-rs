use crate::bindings::*;
use std::pin::Pin;

// TODO: Make Label an Enum

enum Attribute {
    Class(ObjectClass),
    Label(Label),
    //ObjectId(???),
}

#[derive(Debug)]
enum AttributePin {
    Class(Pin<Box<CK_ULONG>>),
    Label(Pin<Box<Vec<u8>>>),
}

#[derive(Default)]
pub struct SearchTemplateBuilder {
    elmts: Vec<Attribute>,
}

impl SearchTemplateBuilder {
    /// Add an attribute to the elements
    fn add_attribute(mut self, elmt: Attribute) -> Self {
        self.elmts.push(elmt);
        self
    }

    /// Add a Label
    pub fn label(mut self, label: Label) -> Self {
        self.add_attribute(Attribute::Label(label))
    }

    /// Add a Class
    pub fn class(mut self, class: ObjectClass) -> Self {
        self.add_attribute(Attribute::Class(class))
    }

    /// Add a class attribute to the template
    fn finalize_class(class: ObjectClass) -> (CK_ATTRIBUTE, AttributePin) {
        let mut class = class.c_val();
        // Make class box here, otherwise the class pointer could have moved already
        let mut class_box = Box::pin(class);
        let class_ptr = &mut *class_box as *mut _ as CK_VOID_PTR;
        (
            CK_ATTRIBUTE {
                type_: CKA_CLASS as CK_ATTRIBUTE_TYPE,
                pValue: class_ptr,
                ulValueLen: (CK_ULONG::BITS / 8) as CK_ULONG,
            },
            AttributePin::Class(class_box),
        )
    }

    /// Add a label attribute to the template
    fn finalize_label(label: Label) -> (CK_ATTRIBUTE, AttributePin) {
        let mut label_bytes = label.c_val();
        // Make Label box, not sure if it matters
        let mut label_box = Box::pin(label_bytes);
        let label_ptr = (*label_box).as_mut_ptr() as CK_VOID_PTR;
        (
            CK_ATTRIBUTE {
                type_: CKA_LABEL as CK_ATTRIBUTE_TYPE,
                pValue: label_ptr,
                ulValueLen: label_box.len() as CK_ULONG,
            },
            AttributePin::Label(label_box),
        )
    }

    /// UNIMPLEMENTED Add an object id attribute to the template
    fn finalize_object_id(mut id: Vec<u8>) -> CK_ATTRIBUTE {
        unimplemented!();
    }

    pub fn build(mut self) -> SearchTemplate {
        let (attributes, pins_) = self
            .elmts
            .into_iter()
            .map(|elmt| match elmt {
                Attribute::Class(class) => Self::finalize_class(class),
                Attribute::Label(label) => Self::finalize_label(label),
            })
            .unzip();
        SearchTemplate { attributes, pins_ }
    }
}

pub struct SearchTemplate {
    pub attributes: Vec<CK_ATTRIBUTE>,
    pins_: Vec<AttributePin>,
}

pub struct ObjectLengthTemplate();
impl ObjectLengthTemplate {
    pub fn new() -> [CK_ATTRIBUTE; 1] {
        [CK_ATTRIBUTE {
            type_: CKA_VALUE as CK_ATTRIBUTE_TYPE,
            pValue: NULL_PTR as CK_VOID_PTR,
            ulValueLen: 0 as CK_ULONG,
        }]
    }
}

pub struct ObjectTemplate();
impl ObjectTemplate {
    pub fn new(value_ptr: CK_VOID_PTR, len: usize) -> [CK_ATTRIBUTE; 1] {
        [CK_ATTRIBUTE {
            type_: CKA_VALUE as CK_ATTRIBUTE_TYPE,
            pValue: value_ptr,
            ulValueLen: len as CK_ULONG,
        }]
    }
}

/// Indicates the signature algorithm and the length of the signature
#[derive(Clone, Copy)]
pub enum SignatureAlgorithm {
    ECDSA(usize),
}

impl SignatureAlgorithm {
    pub fn val(&self) -> u32 {
        match self {
            SignatureAlgorithm::ECDSA(_) => CKM_ECDSA,
            _ => unimplemented!(),
        }
    }
}

pub struct ECDSAMechanism();
impl ECDSAMechanism {
    pub fn new() -> CK_MECHANISM {
        CK_MECHANISM {
            mechanism: CKM_ECDSA as CK_MECHANISM_TYPE,
            pParameter: std::ptr::null_mut() as CK_VOID_PTR,
            ulParameterLen: 0,
        }
    }
}

/// Flags Macro
use bitflags::bitflags;
macro_rules! flags {
    ($flags_name:ident : $($flag:ident),*) => {
        bitflags! {
            pub struct $flags_name: u32 {
                $(
                    const $flag = $flag;
                    )*
            }
        }
    }
}
/// Session Flags
flags!(SessionFlags: CKF_RW_SESSION, CKF_SERIAL_SESSION);

/// MechanismInfo Flags
flags!(
    MechanismInfoFlags: CKF_HW,
    CKF_ENCRYPT,
    CKF_DECRYPT,
    CKF_DIGEST,
    CKF_SIGN,
    CKF_SIGN_RECOVER,
    CKF_VERIFY,
    CKF_VERIFY_RECOVER,
    CKF_GENERATE,
    CKF_GENERATE_KEY_PAIR,
    CKF_WRAP,
    CKF_UNWRAP,
    CKF_DERIVE,
    CKF_EXTENSION
);

/// TokenInfo.flags
flags!(
    TokenInfoFlags: CKF_RNG,
    CKF_WRITE_PROTECTED,
    CKF_LOGIN_REQUIRED,
    CKF_USER_PIN_INITIALIZED,
    CKF_RESTORE_KEY_NOT_NEEDED,
    CKF_CLOCK_ON_TOKEN,
    CKF_PROTECTED_AUTHENTICATION_PATH,
    CKF_DUAL_CRYPTO_OPERATIONS,
    CKF_TOKEN_INITIALIZED,
    CKF_SECONDARY_AUTHENTICATION,
    CKF_USER_PIN_COUNT_LOW,
    CKF_USER_PIN_FINAL_TRY,
    CKF_USER_PIN_LOCKED,
    CKF_USER_PIN_TO_BE_CHANGED,
    CKF_SO_PIN_COUNT_LOW,
    CKF_SO_PIN_FINAL_TRY,
    CKF_SO_PIN_LOCKED,
    CKF_SO_PIN_TO_BE_CHANGED
);

/// Trait for turning labels into &str
pub trait StringLabel {
    fn as_str(&self) -> &str;
}
/// Trait for mapping labels to C values
pub trait CEquiv<T> {
    fn c_val(&self) -> T;
}

/// Object Classes
#[derive(Debug, Clone)]
pub enum ObjectClass {
    Data,
    Certificate,
    PublicKey,
    PrivateKey,
    SecretKey,
    HWFeature,
    DomainParameters,
    Mechanism,
    VendorDefined,
}

impl CEquiv<CK_ULONG> for ObjectClass {
    fn c_val(&self) -> CK_ULONG {
        let val = match self {
            Self::Data => CKO_DATA,
            Self::Certificate => CKO_CERTIFICATE,
            Self::PublicKey => CKO_PUBLIC_KEY,
            Self::PrivateKey => CKO_PRIVATE_KEY,
            Self::SecretKey => CKO_SECRET_KEY,
            Self::HWFeature => CKO_HW_FEATURE,
            Self::DomainParameters => CKO_DOMAIN_PARAMETERS,
            Self::Mechanism => CKO_MECHANISM,
            Self::VendorDefined => CKO_VENDOR_DEFINED,
        };
        val as CK_ULONG
    }
}

/// User Types
#[derive(Debug, Clone)]
pub enum UserType {
    SO,
    User,
}

impl CEquiv<CK_USER_TYPE> for UserType {
    fn c_val(&self) -> CK_USER_TYPE {
        let val = match self {
            Self::SO => CKU_SO,
            Self::User => CKU_USER,
        };
        val as CK_USER_TYPE
    }
}

/// BeID Card Data Labels
/// These are not always the same as the TLV, hence why we need a seperate Enum
/// Names from https://raw.githubusercontent.com/Fedict/eid-mw/master/doc/sdk/documentation/beidsdk_card_data.pdf
/// Last consulted on 2022-09-22
#[derive(Debug, Clone)]
#[allow(non_camel_case_types)]
pub enum Label {
    // Card
    Card,
    // Unparsed Files
    DATA_FILE,
    ADDRESS_FILE,
    PHOTO_FILE,
    SIGN_DATA_FILE,
    SIGN_ADDRESS_FILE,
    BASIC_KEY_FILE,
    // Parsed Files
    CardNumber,
    ChipNumber,
    ValidityBeginDate,
    ValidityEndDate,
    IssuingMunicipality,
    NationalNumber,
    Surname,
    Firstnames,
    FirstLetterOfThirdGivenName,
    Nationality,
    LocationOfBirth,
    DateOfBirth,
    Gender,
    Nobility,
    DocumentType,
    SpecialStatus,
    PhotoHash,
    Duplicata, //sic
    SpecialOrganization,
    MemberOfFamilty,
    DateAndCountryOfProtection,
    WorkPermitMention,
    EmployerVat1,
    EmployerVat2,
    RegionalFileNumber,
    BasicKeyHash,
    BrexitMention1,
    BrexitMention2,
    CardAMention1,
    CardAMention2,
    CardEUStartDate,
    // Address file
    AddressStreetAndNumber,
    AddressZip,
    AddressMunicipality,
    // RN Certificate
    CERT_RN_FILE,
    // Per record Data and Address Files, only 1.8
    Record(Record),
    // Card Info
    CARD_DATA,
    Carddata(Carddata),
    ATR,
    TokenInfoGraphPersoVersion,
    TokenInfoElecPersoVersion,
    TokenInfoElecPersoIntVersion,
    PersoVersions,
}

impl StringLabel for Label {
    fn as_str(&self) -> &str {
        match self {
            Self::Card => "Card",
            // Unparsed Files
            Self::DATA_FILE => "DATA_FILE",
            Self::ADDRESS_FILE => "ADDRESS_FILE",
            Self::PHOTO_FILE => "PHOTO_FILE",
            Self::SIGN_DATA_FILE => "SIGN_DATA_FILE",
            Self::SIGN_ADDRESS_FILE => "SIGN_ADDRESS_FILE",
            Self::BASIC_KEY_FILE => "BASIC_KEY_FILE",
            // Parsed Files
            Self::CardNumber => "card_number",
            Self::ChipNumber => "chip_number",
            Self::ValidityBeginDate => "validity_begin_data",
            Self::ValidityEndDate => "validity_end_date",
            Self::IssuingMunicipality => "issuing_municipality",
            Self::NationalNumber => "national_number",
            Self::Surname => "surname",
            Self::Firstnames => "firstnames",
            Self::FirstLetterOfThirdGivenName => "first_letter_of_third_given_name",
            Self::Nationality => "nationality",
            Self::LocationOfBirth => "location_of_birth",
            Self::DateOfBirth => "date_of_birth",
            Self::Gender => "gender",
            Self::Nobility => "nobility",
            Self::DocumentType => "document_type",
            Self::SpecialStatus => "special_status",
            Self::PhotoHash => "photo_hash",
            Self::Duplicata => "duplicata", //sic
            Self::SpecialOrganization => "special_organization",
            Self::MemberOfFamilty => "member_of_family",
            Self::DateAndCountryOfProtection => "date_and_country_of_protection",
            Self::WorkPermitMention => "work_permit_mention",
            Self::EmployerVat1 => "employer_vat_1",
            Self::EmployerVat2 => "employer_vat_2",
            Self::RegionalFileNumber => "regional_file_number",
            Self::BasicKeyHash => "basic_key_hash",
            Self::BrexitMention1 => "brexit_mention_1",
            Self::BrexitMention2 => "brexit_mention_2",
            Self::CardAMention1 => "cardA_mention_1",
            Self::CardAMention2 => "cardA_mention_2",
            Self::CardEUStartDate => "cardEU_start_date",
            // Address file
            Self::AddressStreetAndNumber => "address_street_and_number",
            Self::AddressZip => "address_zip",
            Self::AddressMunicipality => "address_municipality",
            // RN Certificate
            Self::CERT_RN_FILE => "CERT_RN_FILE",
            // Per record Data and Address Files, only 1.8
            Self::Record(record) => record.as_str(),
            // Card Info
            Self::CARD_DATA => "CARD_DATA",
            Self::Carddata(data) => data.as_str(),
            Self::ATR => "ATR",
            Self::TokenInfoGraphPersoVersion => "tokeninfo_graph_perso_version",
            Self::TokenInfoElecPersoVersion => "tokeninfo_elec_perso_version",
            Self::TokenInfoElecPersoIntVersion => "tokeninfo_elec_perso_int_version",
            Self::PersoVersions => "perso_versions",
        }
    }
}

impl CEquiv<Vec<u8>> for Label {
    fn c_val(&self) -> Vec<u8> {
        self.as_str().to_string().into_bytes()
    }
}

#[derive(Debug, Clone)]
pub enum Record {
    CardNumber,
    CardchipNumber,
    ValidityBeginDate,
    ValidityEndDate,
    IssuingMunicipality,
    NationalNumber,
    Surname,
    Firstnames,
    FirstLetterOfThirdGivenName,
    Nationality,
    LocationOfBirth,
    DateOfBirth,
    Gender,
    Nobility,
    DocumentType,
    SpecialStatus,
    PhotoHash,
    Duplicata, //sic
    SpecialOrganization,
    MemberOfFamilty,
    DateAndCountryOfProtection,
    WorkPermitMention,
    EmployerVat1,
    EmployerVat2,
    RegionalFileNumber,
    BasicKeyHash,
    BrexitMention1,
    BrexitMention2,
    CardAMention1,
    CardAMention2,
    CardEUStartDate,
}

impl StringLabel for Record {
    fn as_str(&self) -> &str {
        match self {
            Self::CardNumber => "record_card_number",
            Self::CardchipNumber => "record_cardchip_number",
            Self::ValidityBeginDate => "record_validity_begin_date",
            Self::ValidityEndDate => "record_validity_end_date",
            Self::IssuingMunicipality => "record_issuing_municipality",
            Self::NationalNumber => "record_national_number",
            Self::Surname => "record_surname",
            Self::Firstnames => "record_firstnames",
            Self::FirstLetterOfThirdGivenName => "record_first_letter_of_third_given_name",
            Self::Nationality => "record_nationality",
            Self::LocationOfBirth => "record_location_of_birth",
            Self::DateOfBirth => "record_date_of_birth",
            Self::Gender => "record_gender",
            Self::Nobility => "record_nobility",
            Self::DocumentType => "record_document_type",
            Self::SpecialStatus => "record_special_status",
            Self::PhotoHash => "record_photo_hash",
            Self::Duplicata => "record_duplicata", //sic
            Self::SpecialOrganization => "record_special_organization",
            Self::MemberOfFamilty => "record_member_of_family",
            Self::DateAndCountryOfProtection => "record_date_and_country_of_protection",
            Self::WorkPermitMention => "record_work_permit_mention",
            Self::EmployerVat1 => "record_employer_vat_1",
            Self::EmployerVat2 => "record_employer_vat_2",
            Self::RegionalFileNumber => "record_regional_file_number",
            Self::BasicKeyHash => "record_basic_key_hash",
            Self::BrexitMention1 => "record_brexit_mention_1",
            Self::BrexitMention2 => "record_brexit_mention_2",
            Self::CardAMention1 => "record_cardA_mention_1",
            Self::CardAMention2 => "record_cardA_mention_2",
            Self::CardEUStartDate => "record_cardEU_start_date",
        }
    }
}

#[derive(Debug, Clone)]
pub enum Carddata {
    Serialnumber,
    CompCode,
    OsNumber,
    OsVersion,
    SoftMaskNumber,
    SoftMaskVersion,
    ApplVersion,
    GlobOsVersion,
    ApplIntVersion,
    Pkcs1Support,
    ApplLifecycle,
    Pkcs15Version,
    KeyExchangeVersion,
    Signature,
}

impl StringLabel for Carddata {
    fn as_str(&self) -> &str {
        match self {
            Self::Serialnumber => "carddata_serialnumber",
            Self::CompCode => "carddata_comp_code",
            Self::OsNumber => "carddata_os_number",
            Self::OsVersion => "carddata_os_version",
            Self::SoftMaskNumber => "carddata_soft_mask_number",
            Self::SoftMaskVersion => "carddata_soft_mask_version",
            Self::ApplVersion => "carddata_appl_version",
            Self::GlobOsVersion => "carddata_glob_os_version",
            Self::ApplIntVersion => "carddata_appl_int_version",
            Self::Pkcs1Support => "carddata_pkcs1_support",
            Self::ApplLifecycle => "carddata_appl_lifecycle",
            Self::Pkcs15Version => "carddata_pkcs15_version",
            Self::KeyExchangeVersion => "carddata_key_exchange_version",
            Self::Signature => "carddata_signature",
        }
    }
}

/// BeID Grouped Card Data Object ID's
/// https://raw.githubusercontent.com/Fedict/eid-mw/master/doc/sdk/documentation/beidsdk_card_data.pdf
/// Last consulted on 2022-09-22
pub enum ObjectID {
    Id,
    Address,
    Photo,
    Carddata,
    Rncert,
    SignDataFile,
    SignAddressFile,
    Tokeninfo,
}

impl CEquiv<Vec<u8>> for ObjectID {
    fn c_val(&self) -> Vec<u8> {
        match self {
            Self::Id => "id",
            Self::Address => "address",
            Self::Photo => "photo",
            Self::Carddata => "carddata",
            Self::Rncert => "rncert",
            Self::SignDataFile => "sign_data_file",
            Self::SignAddressFile => "sign_address_file",
            Self::Tokeninfo => "token_info",
        }
        .to_string()
        .into_bytes()
    }
}

/// Enum for each possible tag in the BeID ID File
/// Official documentation can be found here: https://raw.githubusercontent.com/Fedict/eid-mw/master/doc/sdk/documentation/Applet%201.8%20eID%20Cards/ID_ADRESSE_File_applet1_8_v4.pdf
/// Document last consulted on 2022-06-21
use crate::files::{Tag, TagTrait, TLV};

#[derive(Debug)]
pub enum IdTag<'a> {
    FileStructureVersion(&'a [u8]),
    CardNumber(&'a str),
    ChipNumber(&'a [u8]),
    CardValidityDateBegin(&'a str),
    CardValidityDateEnd(&'a str),
    CardDeliveryMunicipality(&'a str),
    NationalNumber(&'a str),
    Name(&'a str),
    TwoFirstGivenNames(&'a str),
    FirstLetterOfThirdGivenName(&'a str),
    Nationality(&'a str),
    BirthLocation(&'a str),
    BirthDate(&'a str),
    Sex(&'a str),
    NobleCondition(&'a str),
    DocumentType(&'a str),
    SpecialStatus(&'a str),
    HashOfPhoto(&'a [u8]),
    Duplicate(&'a str),
    SpecialOrganisation(&'a str),
    MemberOfFamilty(bool),
    DateAndCountryOfProtection(&'a str),
    MentionsLabourMarket(&'a str),
    VatNumber1(&'a str),
    VatNumber2(&'a str),
    RegionalFileNumber(&'a str),
    HashOfPuk1(&'a [u8]),
    MentionBrexitArticle18Agreement(&'a str),
    MentionBrexitPermanentResidence(&'a str),
    MentionsEducation(&'a str),
    MentionMobilityProgram(&'a str),
    RegistrationDateEU(&'a str),
    Unimplemented(),
}

use std::str::{self, Utf8Error};
impl<'a> TagTrait<'a> for IdTag<'a> {
    /// Convert a byte tag and its byte data to a Tag
    fn from_byte_vec(tag: u8, bytes: &'a [u8]) -> Result<Tag, Utf8Error> {
        let s = match tag {
            0 => Self::FileStructureVersion(bytes),
            1 => Self::CardNumber(str::from_utf8(bytes)?),
            2 => Self::ChipNumber(bytes),
            3 => Self::CardValidityDateBegin(str::from_utf8(bytes)?),
            4 => Self::CardValidityDateEnd(str::from_utf8(bytes)?),
            5 => Self::CardDeliveryMunicipality(str::from_utf8(bytes)?),
            6 => Self::NationalNumber(str::from_utf8(bytes)?),
            7 => Self::Name(str::from_utf8(bytes)?),
            8 => Self::TwoFirstGivenNames(str::from_utf8(bytes)?),
            9 => Self::FirstLetterOfThirdGivenName(str::from_utf8(bytes)?),
            10 => Self::Nationality(str::from_utf8(bytes)?),
            11 => Self::BirthLocation(str::from_utf8(bytes)?),
            12 => Self::BirthDate(str::from_utf8(bytes)?), // TODO: parse the encoded value
            13 => Self::Sex(str::from_utf8(bytes)?),
            14 => Self::NobleCondition(str::from_utf8(bytes)?),
            15 => Self::DocumentType(str::from_utf8(bytes)?),
            16 => Self::SpecialStatus(str::from_utf8(bytes)?),
            17 => Self::HashOfPhoto(bytes),
            18 => Self::Duplicate(str::from_utf8(bytes)?),
            19 => Self::SpecialOrganisation(str::from_utf8(bytes)?),
            20 => {
                let b = if (bytes[0] == 0) { false } else { true };
                Self::MemberOfFamilty(b)
            }
            21 => Self::DateAndCountryOfProtection(str::from_utf8(bytes)?),
            22 => Self::MentionsLabourMarket(str::from_utf8(bytes)?),
            23 => Self::VatNumber1(str::from_utf8(bytes)?),
            24 => Self::VatNumber2(str::from_utf8(bytes)?),
            25 => Self::RegionalFileNumber(str::from_utf8(bytes)?),
            26 => Self::HashOfPuk1(bytes),
            27 => Self::MentionBrexitArticle18Agreement(str::from_utf8(bytes)?),
            28 => Self::MentionBrexitPermanentResidence(str::from_utf8(bytes)?),
            29 => Self::MentionsEducation(str::from_utf8(bytes)?),
            30 => Self::MentionMobilityProgram(str::from_utf8(bytes)?),
            31 => Self::RegistrationDateEU(str::from_utf8(bytes)?),
            _ => Self::Unimplemented(),
        };
        Ok(Tag::ID(s))
    }
}

/// The full IdFile
/// TODO: find a better way for storing or representing the values.
#[derive(Debug)]
pub struct IdFile<'a> {
    raw_data: &'a [u8],
    pub tlvs: Vec<TLV<'a>>,
}

impl<'a> IdFile<'a> {
    /// Convert TLV bytes to an IdFile
    pub fn from_tlv(raw_data: &'a [u8]) -> Self {
        let tlvs = Self::parse_tlv(raw_data);
        Self { raw_data, tlvs }
    }

    /// Parse the TLV file
    //TODO: Parse this in a nicer, more modern manner
    fn parse_tlv(bytes: &'a [u8]) -> Vec<TLV<'a>> {
        let mut res = vec![];
        let mut i = 0;
        while i < bytes.len() {
            // Get tag
            let tag = bytes[i];
            // Get length of length and length
            let (ll, l) = Self::get_length(bytes, i + 1);
            // Calculate final index
            let end_idx = i + 1 + ll + l;
            // Get value bytes
            let v = &bytes[i + 1 + ll..end_idx];
            //Construct and push tlv
            let tag = IdTag::from_byte_vec(tag, v).unwrap();
            let tlv = TLV {
                tag,
                length: l,
                tag_idx: i,
                length_idx: i + 1,
                value_idx: i + 1 + ll,
            };
            res.push(tlv);
            //Increment i and skip zeros
            i = Self::skip_zeros(bytes, end_idx);
        }
        res
    }

    /// Calculate the length at the given position
    ///
    /// Arguments:
    /// - bytes: The TLV bytes
    /// - l_idx: The index of the length byte(s)
    ///
    /// Returns:
    /// - number of length bytes
    /// - length value calculated with the length bytes
    fn get_length(bytes: &[u8], l_idx: usize) -> (usize, usize) {
        let mut cnt = 0;
        while bytes[l_idx + cnt] == 0xFF {
            cnt = cnt + 1;
        }
        (cnt + 1, cnt * 255 + bytes[l_idx + cnt] as usize)
    }

    /// Return the first non-zero index starting from the given index
    fn skip_zeros(bytes: &[u8], idx: usize) -> usize {
        let mut idx = idx;
        while (idx < bytes.len()) && (bytes[idx] == 0) {
            idx = idx + 1;
        }
        idx
    }
}

impl<'a> IdFile<'a> {
    /// Allow iteration over TLV's in an IDFile
    pub fn iter(&self) -> impl Iterator<Item = &TLV<'a>> {
        self.tlvs.iter()
    }
}

/// Enum for each possible tag in the BeID Address File
/// Official documentation can be found here: https://raw.githubusercontent.com/Fedict/eid-mw/master/doc/sdk/documentation/Applet%201.8%20eID%20Cards/ID_ADRESSE_File_applet1_8_v4.pdf
/// Document last consulted on 2022-06-21
use crate::files::{Tag, TagTrait, TLV};

#[derive(Debug)]
pub enum AddressTag<'a> {
    FileStructureVersion(&'a [u8]),
    StreetAndNumber(&'a str),
    ZipCode(&'a str),
    Municipality(&'a str),
    Unimplemented(),
}

use std::str::{self, Utf8Error};
impl<'a> TagTrait<'a> for AddressTag<'a> {
    /// Convert a byte tag and its byte data to a Tag
    fn from_byte_vec(tag: u8, bytes: &'a [u8]) -> Result<Tag, Utf8Error> {
        let s = match tag {
            0 => Self::FileStructureVersion(bytes),
            1 => Self::StreetAndNumber(str::from_utf8(bytes)?),
            2 => Self::ZipCode(str::from_utf8(bytes)?),
            3 => Self::Municipality(str::from_utf8(bytes)?),
            _ => Self::Unimplemented(),
        };
        Ok(Tag::Address(s))
    }
}

/// The full AddressFile
/// TODO: find a better way for storing or representing the values.
/// TODO: Code duplication with IdFile
#[derive(Debug)]
pub struct AddressFile<'a> {
    raw_data: &'a [u8],
    tlvs: Vec<TLV<'a>>,
}

impl<'a> AddressFile<'a> {
    /// Convert TLV bytes to an IdFile
    pub fn from_tlv(raw_data: &'a [u8]) -> Self {
        let tlvs = Self::parse_tlv(raw_data);
        Self { raw_data, tlvs }
    }

    /// Parse the TLV file
    //TODO: Parse this in a nicer, more modern manner
    fn parse_tlv(bytes: &'a [u8]) -> Vec<TLV<'a>> {
        let mut res = vec![];
        let mut i = 0;
        while i < bytes.len() {
            // Get tag
            let tag = bytes[i];
            // Get length of length and length
            let (ll, l) = Self::get_length(bytes, i + 1);
            // Calculate final index
            let end_idx = i + 1 + ll + l;
            // Get value bytes
            let v = &bytes[i + 1 + ll..end_idx];
            //Construct and push tlv
            let tag = AddressTag::from_byte_vec(tag, v).unwrap();
            let tlv = TLV {
                tag,
                length: l,
                tag_idx: i,
                length_idx: i + 1,
                value_idx: i + 1 + ll,
            };
            res.push(tlv);
            //Increment i and skip zeros
            i = Self::skip_zeros(bytes, end_idx);
        }
        res
    }

    /// Calculate the length at the given position
    ///
    /// Arguments:
    /// - bytes: The TLV bytes
    /// - l_idx: The index of the length byte(s)
    ///
    /// Returns:
    /// - number of length bytes
    /// - length value calculated with the length bytes
    fn get_length(bytes: &[u8], l_idx: usize) -> (usize, usize) {
        let mut cnt = 0;
        while bytes[l_idx + cnt] == 0xFF {
            cnt = cnt + 1;
        }
        (cnt + 1, cnt * 255 + bytes[l_idx + cnt] as usize)
    }

    /// Return the first non-zero index starting from the given index
    fn skip_zeros(bytes: &[u8], idx: usize) -> usize {
        let mut idx = idx;
        while (idx < bytes.len()) && (bytes[idx] == 0) {
            idx = idx + 1;
        }
        idx
    }
}

impl<'a> AddressFile<'a> {
    /// Allow iteration over TLV's in an AddressFile
    pub fn iter(&self) -> impl Iterator<Item = &TLV<'a>> {
        self.tlvs.iter()
    }
}

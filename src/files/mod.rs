pub mod address_file;
pub mod id_file;

pub use address_file::{AddressFile, AddressTag};
pub use id_file::{IdFile, IdTag};

#[derive(Debug)]
pub enum Tag<'a> {
    ID(IdTag<'a>),
    Address(AddressTag<'a>),
}

use std::str::{self, Utf8Error};
pub trait TagTrait<'a> {
    fn from_byte_vec(tag: u8, bytes: &'a [u8]) -> Result<Tag<'a>, Utf8Error>;
}

/// A single TLV tag with metadata
#[derive(Debug)]
pub struct TLV<'a> {
    pub tag_idx: usize,
    pub length_idx: usize,
    pub value_idx: usize,
    pub tag: Tag<'a>,
    pub length: usize,
}

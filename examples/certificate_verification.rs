use beid::errors::Error;
use beid::*;
use webpki::{EndEntityCert, ECDSA_P384_SHA384, RSA_PKCS1_2048_8192_SHA256, TrustAnchor, Time, TlsClientTrustAnchors};

fn main() -> Result<(), Error> {
    // Setup
    let interface = BeID::new()?;
    interface.initialize()?;
    let slot = interface.get_slot_list(true)?[0];
    let session = interface.open_session(slot, SessionFlags::CKF_SERIAL_SESSION)?;

    // Get data
    let template = SearchTemplateBuilder::default()
        .class(ObjectClass::Data)
        .label(Label::DATA_FILE)
        .build();
    let msg_o = session.search_objects(template, 1)?;
    let msg = session.get_attribute_value(msg_o[0])?;
    // Get signature
    let template = SearchTemplateBuilder::default()
        .class(ObjectClass::Data)
        .label(Label::SIGN_DATA_FILE)
        .build();
    let sign_o = session.search_objects(template, 1)?;
    let sign = session.get_attribute_value(sign_o[0])?;
    // Root CA of test cards
    // RRN Certificate, not a ObjectClass::Certificate on the card
    // It has the label "CERT_RN_FILE"
    let template = SearchTemplateBuilder::default()
        .label(Label::CERT_RN_FILE)
        .build();
    let cert_o = session.search_objects(template, 10)?;
    let mut rrn_cert = session.get_attribute_value(cert_o[0])?;
    // Cert has a bunch of 0's at the end that webpki can't deal with
    let tailing_zeros = rrn_cert.iter().rev().position(|x| x.clone() != 0u8).unwrap();
    rrn_cert.truncate(rrn_cert.len() - tailing_zeros);
    //Print
    //println!("Data ({}):\n{:x?}", msg.len(), msg);
    //println!("Sign ({}):\n{:x?}", sign.len(), sign);
    //println!("Cert ({}):\n{:x?}", cert.len(), &cert);

    // See what mechanims are available
    let mechanisms = interface.get_mechanism_list(slot).unwrap();
    // Verification
    let cert = EndEntityCert::try_from(rrn_cert.as_slice()).unwrap();
    let time = Time::try_from(std::time::SystemTime::now()).unwrap();
    let (test_root_bytes, protocol) = if mechanisms.contains(&(SignatureAlgorithm::ECDSA(0).val() as u64)) {
        (include_bytes!("certs/testcards_be_root_EC.crt").to_vec(), &ECDSA_P384_SHA384)
    } else {
        (include_bytes!("certs/testcards_be_root.crt").to_vec(), &RSA_PKCS1_2048_8192_SHA256)
    };
    // Cert has a bunch of 0's at the end that webpki can't deal with
    let tailing_zeros = test_root_bytes.iter().rev().position(|x| x.clone() != 0u8).unwrap();
    let test_root_bytes = test_root_bytes[..test_root_bytes.len() - tailing_zeros].to_vec();
    let test_root_cert = [TrustAnchor::try_from_cert_der(&test_root_bytes).unwrap()];
    let anchors = TlsClientTrustAnchors(&test_root_cert);
    // Verify signature
    let sign_verif = cert.verify_signature(protocol, msg.as_slice(), sign.as_slice());
    let valid_client_cert = cert.verify_is_valid_tls_client_cert(
        &[protocol],
        &anchors,
        &[],
        time);
    match sign_verif.and(valid_client_cert) {
        Err(e) => println!("Could not verify signature: {:?}", e),
        Ok(()) => println!("Signature verified correctly"),
    };
    Ok(())
}

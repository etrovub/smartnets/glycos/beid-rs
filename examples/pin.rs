use beid::*;

fn main() {
    // Create interface
    let interface = BeID::new().unwrap();
    // Initialize
    interface.initialize().unwrap();
    // Get slots
    let slots = interface.get_slot_list(true).unwrap();

    // Read data from slots
    for slot in slots {
        // Open session
        let session = interface
            .open_session(slot, SessionFlags::CKF_SERIAL_SESSION)
            .unwrap();
        // See if there's a way to authenticate without PIN
        let token_info = interface.get_token_info(slot).unwrap();
        let flags = TokenInfoFlags::from_bits(token_info.flags as u32).unwrap();
        if !flags.contains(TokenInfoFlags::CKF_PROTECTED_AUTHENTICATION_PATH) {
            // Log in with PIN
            // Ask PIN
            println!("Enter PIN:");
            let mut pin = String::new();
            std::io::stdin().read_line(&mut pin).unwrap();
            let mut pin = pin.into_bytes();
            if pin.len() < 2 {
                println!("No pin entered");
                return;
            }
            pin.truncate(pin.len() - 1);
            // Login
            match session.login(UserType::User, Some(&mut pin)) {
                Ok(()) => println!("Logged in succesfully!"),
                Err(e) => {
                    println!("Failed to log in: {:?}", e);
                }
            };
        } else {
            // Log in without PIN
            match session.login(UserType::User, None) {
                Ok(()) => println!("Logged in succesfully!"),
                Err(e) => {
                    println!("Failed to log in: {:?}", e);
                }
            };
        }
    }
}

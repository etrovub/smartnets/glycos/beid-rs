use beid::errors::Error;
use beid::files::{AddressFile, IdFile, IdTag, Tag};
use beid::*;

fn main() -> Result<(), Error> {
    // Setup
    let interface = BeID::new()?;
    interface.initialize()?;
    let slot = interface.get_slot_list(true)?[0];
    let session = interface.open_session(slot, SessionFlags::CKF_SERIAL_SESSION)?;

    // Get ID data
    let template = SearchTemplateBuilder::default()
        .class(ObjectClass::Data)
        .label(Label::DATA_FILE)
        .build();
    let data_o = session.search_objects(template, 1)?;
    let data = session.get_attribute_value(data_o[0])?;

    // Parse ID data
    let id = IdFile::from_tlv(&data);
    println!("### ID FILE ###");
    for tlv in id.iter() {
        println!("{:?}", tlv.tag);
    }

    // Get Address data
    let template = SearchTemplateBuilder::default()
        .class(ObjectClass::Data)
        .label(Label::ADDRESS_FILE)
        .build();
    let data_o = session.search_objects(template, 1)?;
    let data = session.get_attribute_value(data_o[0])?;

    // Parse Address data
    let address = AddressFile::from_tlv(&data);
    println!("\n### ADDRESS FILE ###");
    for tlv in address.iter() {
        println!("{:?}", tlv.tag);
    }

    // Find a specific value
    println!("\n### FIRST NAMES ###");
    let names = id.iter().find_map(|x| match x.tag {
        Tag::ID(IdTag::TwoFirstGivenNames(names)) => Some(names),
        _ => None,
    });
    match names {
        Some(names) => println!("Found first names {}", names),
        None => println!("Did not find first names"),
    };
    Ok(())
}

use beid::*;

fn main() {
    // Create interface
    let interface = BeID::new().unwrap();
    // Initialize
    interface.initialize().unwrap();
    // Get slots
    let slots = interface.get_slot_list(true).unwrap();

    // Read data from slots
    for slot in slots {
        // Open session
        let session = interface
            .open_session(slot, SessionFlags::CKF_SERIAL_SESSION)
            .unwrap();
        // Build template for Serial Number
        let template = SearchTemplateBuilder::default()
            .label(Label::Carddata(Carddata::Serialnumber))
            .class(ObjectClass::Data)
            .build();
        // Find serial number
        let objects = session.search_objects(template, 1).unwrap();
        let snr = session.get_attribute_value(objects[0]).unwrap();

        // Build template for Certificate
        let template = SearchTemplateBuilder::default()
            .class(ObjectClass::Certificate)
            .build();
        let objects = session.search_objects(template, 1).unwrap();
        let cert = session.get_attribute_value(objects[0]).unwrap();

        // Print output:
        println!("Serial number:");
        for b in snr {
            let c = char::from_u32(b as u32).unwrap_or('.');
            print!("{}", c);
        }
        println!("\nCertificate:");
        println!("{:x?}", cert);
    }
}

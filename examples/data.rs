use beid::*;

fn main() {
    // Create interface
    let interface = BeID::new().unwrap();
    // Initialize
    interface.initialize().unwrap();
    // Get slots
    let slots = interface.get_slot_list(true).unwrap();

    // Read data from slots
    for slot in slots {
        // Open session
        let session = interface
            .open_session(slot, SessionFlags::CKF_SERIAL_SESSION)
            .unwrap();
        // Search 3 things
        for label in [
            Label::Carddata(Carddata::GlobOsVersion),
            Label::CARD_DATA,
            Label::Surname,
        ] {
            // Create the template
            let template = SearchTemplateBuilder::default()
                .label(label.clone())
                .class(ObjectClass::Data)
                .build();
            // Find the objects
            let objects = session.search_objects(template, 1).unwrap();
            // Print the objects
            for o in objects {
                let data = session.get_attribute_value(o).unwrap();
                print!("{:?} ({}): ", label, data.len());
                match std::str::from_utf8(data.as_slice()) {
                    Ok(s) => println!("{}", s),
                    _ => println!("*byte data*"),
                }
            }
        }
    }
}

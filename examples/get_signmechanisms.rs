use beid::*;

fn main() {
    // Create interface
    let interface = BeID::new().unwrap();
    // Initialize
    interface.initialize().unwrap();
    // Get slots
    let slots = interface.get_slot_list(true).unwrap();

    // Read data from slots
    for slot in slots {
        // Get available mechanisms
        let mechanisms = interface.get_mechanism_list(slot).unwrap();
        for mechanism in mechanisms {
            let info = interface.get_mechanism_info(slot, mechanism).unwrap();
            let flags = MechanismInfoFlags::from_bits_truncate(info.flags as u32);
            println!(
                "Mechanism {}: Key Length: [{}, {}]",
                mechanism, info.ulMinKeySize, info.ulMaxKeySize
            );
            println!("    Flags: {:?}", flags);
        }
    }
}
